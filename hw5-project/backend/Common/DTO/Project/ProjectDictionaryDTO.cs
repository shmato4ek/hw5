﻿namespace Common.DTO
{
    public class ProjectDictionaryDTO
    {
        public ProjectDTO Key { get; set; }
        public int Value { get; set; }
    }
}

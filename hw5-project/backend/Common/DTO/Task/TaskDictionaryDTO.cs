﻿namespace Common.DTO
{
    public class TaskDictionaryDTO
    {
        public int Key { get; set; }
        public string Value { get; set; }
    }
}

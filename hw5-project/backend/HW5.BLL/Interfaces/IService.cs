﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace HW5.BLL.Interfaces
{
    public interface IService<T, K>
    {
        Task<List<T>> GetAll();
        Task<T> Get(int id);
        Task<T> Add(K item);
        Task<T> Update(T item);
        Task Delete(int id);
    }
}

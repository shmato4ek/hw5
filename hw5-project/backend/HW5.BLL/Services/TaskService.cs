﻿using AutoMapper;
using Common.DTO;
using HW5.BLL.Interfaces;
using HW5.BLL.Services.Abstract;
using HW5.DAL.UOW;
using System.Collections.Generic;
using HW5.DAL.Entities;
using System.Threading.Tasks;
using Common.DTO.Task;

namespace HW5.BLL.Services
{
    public class TaskService : BaseService, IService<TaskDTO, TaskCreatedDTO>
    {
        public TaskService(IMapper mapper, IUnitOfWork uow) : base(mapper, uow) { }

        public async Task<List<TaskDTO>> GetAll()
        {
            var result = await data.Tasks.GetAll();

            return _mapper.Map<List<TaskDTO>>(result);
        }

        public async Task<TaskDTO> Get(int id)
        {
            var task = await data.Tasks.Get(id);

            return _mapper.Map<TaskDTO>(task);
        }

        public async Task<TaskDTO> Add(TaskCreatedDTO task)
        {
            var newTask = _mapper.Map<TaskEntity>(task);
            await data.Tasks.Create(newTask);
            await data.Save();

            return _mapper.Map<TaskDTO>(await data.Tasks.Get(newTask.Id));
        }

        public async Task<TaskDTO> Update(TaskDTO task)
        {
            var newTask = _mapper.Map<TaskEntity>(task);
            await data.Tasks.Update(newTask);
            await data.Save();

            return _mapper.Map<TaskDTO>(await data.Tasks.Get(newTask.Id));
        }

        public async Task Delete(int id)
        {
            await data.Tasks.Delete(id);
            await data.Save();
        }
    }
}

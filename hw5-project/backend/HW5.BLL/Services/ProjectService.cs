﻿using AutoMapper;
using Common.DTO;
using HW5.BLL.Interfaces;
using HW5.BLL.Services.Abstract;
using HW5.DAL.UOW;
using System.Collections.Generic;
using HW5.DAL.Entities;
using System.Threading.Tasks;
using Common.DTO.Project;

namespace HW5.BLL.Services
{
    public class ProjectService : BaseService, IService<ProjectDTO, ProjectCreatedDTO>
    {
        public ProjectService(IMapper mapper, IUnitOfWork uow) : base(mapper, uow) { }

        public async Task<List<ProjectDTO>> GetAll()
        {
            var result = await data.Projects.GetAll();

            return _mapper.Map<List<ProjectDTO>>(result);
        }

        public async Task<ProjectDTO> Get(int id)
        {
            var project = await data.Projects.Get(id);

            return _mapper.Map<ProjectDTO>(project);
        }

        public async Task<ProjectDTO> Add(ProjectCreatedDTO project)
        {
            var newProject = _mapper.Map<ProjectEntity>(project);
            await data.Projects.Create(newProject);
            await data.Save();

            return _mapper.Map<ProjectDTO>(await data.Projects.Get(newProject.Id));
        }

        public async Task<ProjectDTO> Update(ProjectDTO project)
        {
            var newProject = _mapper.Map<ProjectEntity>(project);
            await data.Projects.Update(newProject);
            await data.Save();

            return _mapper.Map<ProjectDTO>(await data.Projects.Get(newProject.Id));
        }

        public async Task Delete(int id)
        {
            await data.Projects.Delete(id);
            await data.Save();
        }
    }
}

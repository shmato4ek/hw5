﻿using AutoMapper;
using Common.DTO;
using HW5.BLL.Interfaces;
using HW5.BLL.Services.Abstract;
using HW5.DAL.UOW;
using System.Collections.Generic;
using HW5.DAL.Entities;
using System.Threading.Tasks;
using Common.DTO.User;

namespace HW5.BLL.Services
{
    public class UserService : BaseService, IService<UserDTO, UserCreatedDTO>
    {
        public UserService(IMapper mapper, IUnitOfWork uow) : base(mapper, uow) { }

        public async Task<List<UserDTO>> GetAll()
        {
            var result = await data.Users.GetAll();

            return _mapper.Map<List<UserDTO>>(result);
        }

        public async Task<UserDTO> Get(int id)
        {
            var user = await data.Users.Get(id);

            return _mapper.Map<UserDTO>(user);
        }

        public async Task<UserDTO> Add(UserCreatedDTO user)
        {
            var newUser = _mapper.Map<UserEntity>(user);
            await data.Users.Create(newUser);
            await data.Save();

            return _mapper.Map<UserDTO>(await data.Users.Get(newUser.Id));
        }

        public async Task<UserDTO> Update(UserDTO user)
        {
            var newUser = _mapper.Map<UserEntity>(user);
            await data.Users.Update(newUser);
            await data.Save();

            return _mapper.Map<UserDTO>(await data.Users.Get(newUser.Id));
        }

        public async Task Delete(int id)
        {
            await data.Users.Delete(id);
            await data.Save();
        }
    }
}

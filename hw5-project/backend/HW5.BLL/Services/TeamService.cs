﻿using AutoMapper;
using Common.DTO;
using HW5.BLL.Services.Abstract;
using HW5.BLL.Interfaces;
using HW5.DAL.Entities;
using HW5.DAL.UOW;
using System.Collections.Generic;
using System.Threading.Tasks;
using Common.DTO.Team;

namespace HW5.BLL.Services
{
    public class TeamService : BaseService, IService<TeamDTO, TeamCreatedDTO>
    {
        public TeamService(IMapper mapper, IUnitOfWork uow) : base(mapper, uow) { }

        public async Task<List<TeamDTO>> GetAll()
        {
            var result = await data.Teams.GetAll();

            return _mapper.Map<List<TeamDTO>>(result);
        }

        public async Task<TeamDTO> Get(int id)
        {
            var team = await data.Teams.Get(id);

            return _mapper.Map<TeamDTO>(team);
        }

        public async Task<TeamDTO> Add(TeamCreatedDTO team)
        {
            var newTeam = _mapper.Map<TeamEntity>(team);
            await data.Teams.Create(newTeam);
            await data.Save();

            return _mapper.Map<TeamDTO>(await data.Teams.Get(newTeam.Id));
        }

        public async Task<TeamDTO> Update(TeamDTO team)
        {
            var newTeam = _mapper.Map<TeamEntity>(team);
            await data.Teams.Update(newTeam);
            await data.Save();

            return _mapper.Map<TeamDTO>(await data.Teams.Get(newTeam.Id));
        }

        public async Task Delete(int id)
        {
            await data.Teams.Delete(id);
            await data.Save();
        }
    }
}

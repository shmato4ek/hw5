﻿using AutoMapper;
using HW5.DAL.UOW;

namespace HW5.BLL.Services.Abstract
{
    public abstract class BaseService
    {
        private protected readonly IMapper _mapper;
        protected IUnitOfWork data { get; set; }
        public BaseService(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            data = unitOfWork;
        }
    }
}

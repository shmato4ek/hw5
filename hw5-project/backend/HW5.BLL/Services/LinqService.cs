﻿using AutoMapper;
using Common.DTO;
using HW5.BLL.Structs;
using HW5.DAL.UOW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HW5.BLL.Services
{
    public class LinqService
    {
        private readonly DataService data;
        private readonly IMapper _mapper;
        private readonly int currentYear = 2021;
        private readonly int maxLengthOfName = 45;
        private readonly int minDescriptionLength = 20;
        private readonly int maxAmmountOfTasks = 3;

        public LinqService (IUnitOfWork uow, IMapper mapper)
        {
            data = new DataService(uow);
            _mapper = mapper;
        }

        public async Task<List<ProjectDictionaryDTO>> GetAmmountOfTasksOfConcreteUser(int userId)
        {
            var projects = await data.GetRequest();
            var foundProjectsWithTasks = projects
                .Where(project => project.Author.Id == userId)
                .Select((project) => new
                {
                    Project = project,
                    AmmountOfTasks = project.Tasks.Count
                })
                .ToDictionary(d => d.Project, d => d.AmmountOfTasks);

            if (!foundProjectsWithTasks.Any())
            {
                throw new Exception("There is no project of this user or id is incorrect");
            }

            var result = new List<ProjectDictionaryDTO>();
            foreach (var item in foundProjectsWithTasks)
            {
                result.Add(new ProjectDictionaryDTO { Key = _mapper.Map<ProjectDTO>(item.Key), Value = item.Value});
            }

            return result;
        }
        public async Task<List<TaskDTO>> GetAllTasksOfConcreteUser(int userId)
        {
            var projects = await data.GetRequest();
            var tasks = projects
                .SelectMany(project => project.Tasks.Where(task => task.Performer.Id == userId && task.Name.Length < maxLengthOfName))
                .ToList();

            if (!tasks.Any())
            {
                throw new Exception("No tasks or ucorrect user id");
            }

            var resultDto = _mapper.Map<List<TaskDTO>>(tasks);
            foreach (var item in resultDto)
            {
                var projectId = projects.Where(project => project.Tasks.Any(task => task.Id == item.Id)).FirstOrDefault().Id;
                item.ProjectId = projectId;
            }

            return resultDto;
        }

        public async Task<List<TaskDictionaryDTO>> GetFinishedTasksOfConcreteUser(int userId)
        {
            var projects = await data.GetRequest();
            var foundTasks = projects
                .SelectMany(project => project.Tasks)
                .Where(task => task.FinishedAt != null && task.Performer.Id == userId && task.FinishedAt.Value.Year == currentYear)
                .Select(task => new
                {
                    Id = task.Id,
                    Name = task.Name
                })
                .Distinct()
                .ToDictionary(d => d.Id, d => d.Name);

            if (!foundTasks.Any())
            {
                throw new Exception("There is no tasks of this user or wrong id");
            }

            var result = new List<TaskDictionaryDTO>();
            foreach(var item in foundTasks)
            {
                result.Add(new TaskDictionaryDTO { Key = item.Key, Value = item.Value});
            }    

            return result;
        }

        public async Task<List<TeamWithUsersStruct>> GetTeamsWithUsersOlderThan10()
        {
            var result = new List<TeamWithUsersStruct>();
            var projects = await data.GetRequest();
            var foundTeams = projects
                .Select(project => project.Team)
                .Where(team => team.Users.All(user => user.Birthday.Value.Year <= 2011))
                .GroupBy(team => team.Id)
                .Select(group => new
                {
                    Id = group.Key,
                    Users = group.Select(team => team.Users).Last().OrderByDescending(user => user.RegisteredAt),
                    Name = group.Select(team => team.Name).Last()
                });

            foreach (var item in foundTeams)
            {
                result.Add(new TeamWithUsersStruct { TeamName = item.Name, Id = item.Id, Users = item.Users });
            }

            return result;
        }

        public async Task<List<UserDictionaryDTO>> GetSortedListOfUsers()
        {
            var projects = await data.GetRequest();
            var foundTasks = projects
                .SelectMany(project => project.Tasks)
                .GroupBy(task => task.Performer.Id)
                .Select(group => new
                {
                    User = group.Select(task => task.Performer).Last(),
                    Tasks = group.OrderByDescending(task => task.Name).ToList()
                })
                .OrderBy(user => user.User.FirstName)
                .ToDictionary(d => d.User, d => d.Tasks);

            var result = new List<UserDictionaryDTO>();
            foreach(var item in foundTasks)
            {
                result.Add(new UserDictionaryDTO { Key = _mapper.Map<UserDTO>(item.Key), Value = _mapper.Map<List<TaskDTO>>(item.Value)});
            }
            
            return result;
        }

        public async Task<UserStructDTO> GetUserStruct(int userId)
        {
            var result = new UserStruct();
            var projects = await data.GetRequest();
            var foundTasks = projects
                .Where(project => project.Author.Id == userId)
                .Select(project => project.Author)
                .GroupJoin(
                projects.SelectMany(project => project.Tasks),
                user => user.Id,
                task => task.Performer.Id,
                (user, tasks) => new
                {
                    User = user,
                    Tasks = tasks
                }
                )
                .GroupJoin(
                projects,
                user => user.User.Id,
                project => project.Author.Id,
                (user, projects) => new
                {
                    User = user.User,
                    Projects = projects,
                    Tasks = user.Tasks
                }
                ).Select(user => new
                {
                    LastProject = user.Projects.OrderBy(project => project.CreatedAt).Last(),
                    User = user.User,
                    AmmoutOfNotFinishedOrClosedTasks = user.Tasks.Where(task => task.FinishedAt == null || task.State == 0).Count(),
                    AmmountOfTaskInLastProject = user.Projects.OrderBy(project => project.CreatedAt).Last().Tasks.Count(),
                    TheLongestTask = user.Tasks.OrderBy(task => task.FinishedAt - task.CreatedAt).LastOrDefault()
                }).First();

            Task mapLastProject = Task.Run(() => result.LastProject = _mapper.Map<ProjectDTO>(foundTasks.LastProject));
            Task notFinishedTasks = Task.Run(() => result.AmmoutOfNotFinishedOrClosedTasks = foundTasks.AmmoutOfNotFinishedOrClosedTasks);
            Task tasksInLastProject = Task.Run(() => result.AmmountOfTaskInLastProject = foundTasks.AmmountOfTaskInLastProject);
            Task mapTheLongestTask = Task.Run(() => result.TheLongestTask = _mapper.Map<TaskDTO>(foundTasks.TheLongestTask));
            Task mapUser = Task.Run(() => result.User = _mapper.Map<UserDTO>(foundTasks.User));

            await Task.WhenAll(new[] { mapLastProject, notFinishedTasks, tasksInLastProject, mapTheLongestTask, mapUser});

            result.User.Id = projects.Where(project => project.Team.Users.Any(user => user.Id == result.User.Id)).FirstOrDefault().Id;

            return _mapper.Map<UserStructDTO>(result);
        }

        public async Task<List<ProjectStruct>> GetProjectStruct()
        {
            var projects = await data.GetRequest();
            var result = new List<ProjectStruct>();
            var resultStruct = projects
                .Select(project => new
                {
                    Project = project,
                    TheShortestTask = project.Tasks.OrderBy(task => task.Name.Length).FirstOrDefault(),
                    TheLongestTask = project.Tasks.OrderBy(task => task.Description.Length).LastOrDefault(),
                    Ammount = (project.Description.Length > minDescriptionLength || project.Tasks.Count() < maxAmmountOfTasks) ? project.Team.Users.Count() : 0
                });

            foreach (var item in resultStruct)
            {
                result.Add(new ProjectStruct { ammountOfUsers = item.Ammount, Project = _mapper.Map<ProjectDTO>(item.Project), theLongestTask = _mapper.Map<TaskDTO>(item.TheLongestTask), theShortestTask = _mapper.Map<TaskDTO>(item.TheShortestTask) });
            }

            return result;
        }

    }
}

﻿using AutoMapper;
using Common.DTO;
using HW5.DAL.Entities;

namespace HW5.BLL.MappingProfiles
{
    public sealed class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<TeamEntity, TeamDTO>();
            CreateMap<TeamDTO, TeamEntity>();
        }
    }
}

﻿using AutoMapper;
using HW5.DAL.Entities;
using Common.DTO.Team;

namespace HW5.BLL.MappingProfiles
{
    public class TeamCreatedProfile : Profile
    {
        public TeamCreatedProfile()
        {
            CreateMap<TeamCreatedDTO, TeamEntity>();
        }
    }
}

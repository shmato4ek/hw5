﻿using AutoMapper;
using Common.DTO;
using HW5.DAL.Entities;

namespace HW5.BLL.MappingProfiles
{
    public sealed class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<TaskEntity, TaskDTO>();
            CreateMap<TaskDTO, TaskEntity>();
        }
    }
}

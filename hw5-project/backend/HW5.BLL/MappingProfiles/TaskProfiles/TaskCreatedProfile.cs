﻿using AutoMapper;
using Common.DTO.Task;
using HW5.DAL.Entities;

namespace HW5.BLL.MappingProfiles
{
    public class TaskCreatedProfile : Profile
    {
        public TaskCreatedProfile()
        {
            CreateMap<TaskCreatedDTO, TaskEntity>();
        }
    }
}

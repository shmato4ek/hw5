﻿using AutoMapper;
using Common.DTO;
using HW5.BLL.ModelsForLINQ;

namespace HW5.BLL.MappingProfiles
{
    public class TaskForLinqProfile : Profile
    {
        public TaskForLinqProfile()
        {
            CreateMap<ProjectTask, TaskDTO>().ForMember(task => task.PerformerId, src => src.MapFrom(member => member.Performer.Id));
            CreateMap<TaskDTO, ProjectTask>();
        }
    }
}

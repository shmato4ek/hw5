﻿using AutoMapper;
using Common.DTO.Project;
using HW5.DAL.Entities;

namespace HW5.BLL.MappingProfiles
{
    public class ProjectCreatedProfile : Profile
    {
        public ProjectCreatedProfile()
        {
            CreateMap<ProjectCreatedDTO, ProjectEntity>();
        }
    }
}

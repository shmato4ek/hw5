﻿using AutoMapper;
using Common.DTO;
using HW5.BLL.ModelsForLINQ;

namespace HW5.BLL.MappingProfiles
{
    public class ProjectLinqProfile : Profile
    {
        public ProjectLinqProfile()
        {
            CreateMap<Project, ProjectDTO>();
        }
    }
}

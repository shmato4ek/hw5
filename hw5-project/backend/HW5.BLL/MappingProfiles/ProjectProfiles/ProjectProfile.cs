﻿using AutoMapper;
using Common.DTO;
using HW5.DAL.Entities;

namespace HW5.BLL.MappingProfiles
{
    public sealed class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<ProjectEntity, ProjectDTO>();
            CreateMap<ProjectDTO, ProjectEntity>();
        }
    }
}

﻿using AutoMapper;
using Common.DTO.User;
using HW5.DAL.Entities;

namespace HW5.BLL.MappingProfiles
{
    public class UserCreatedProfile : Profile
    {
        public UserCreatedProfile()
        {
            CreateMap<UserCreatedDTO, UserEntity>();
        }
    }
}

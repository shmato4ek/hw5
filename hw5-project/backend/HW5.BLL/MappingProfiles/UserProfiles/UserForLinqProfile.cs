﻿using AutoMapper;
using Common.DTO;
using HW5.BLL.ModelsForLINQ;

namespace HW5.BLL.MappingProfiles
{
    public class UserForLinqProfile : Profile
    {
        public UserForLinqProfile()
        {
            CreateMap<User, UserDTO>();
        }
    }
}

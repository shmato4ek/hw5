﻿using AutoMapper;
using Common.DTO;
using HW5.BLL.Structs;

namespace HW5.BLL.MappingProfiles
{
    public class UserStructProfile : Profile
    {
        public UserStructProfile()
        {
            CreateMap<UserStruct, UserStructDTO>();
        }
    }
}

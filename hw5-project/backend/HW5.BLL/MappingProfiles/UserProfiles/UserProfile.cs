﻿using AutoMapper;
using Common.DTO;
using HW5.DAL.Entities;

namespace HW5.BLL.MappingProfiles
{
    public sealed class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserEntity, UserDTO>();
            CreateMap<UserDTO, UserEntity>();
        }
    }
}

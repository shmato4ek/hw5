﻿using HW5.DAL.EF;
using HW5.DAL.Entities;
using HW5.DAL.Repository;
using System.Threading.Tasks;

namespace HW5.DAL.UOW
{
    public class UnitOfWork : IUnitOfWork
    {
        private ProjectsContext _context;
        private UserRepository userRepository;
        private ProjectRepository projectRepository;
        private TaskRepository taskRepository;
        private TeamRepository teamRepository;
        public UnitOfWork (ProjectsContext context)
        {
            _context = context;
        }
        public IRepository<UserEntity> Users
        {
            get
            {
                if (userRepository == null)
                {
                    userRepository = new UserRepository(_context);
                }
                return userRepository;
            }
        }

        public IRepository<ProjectEntity> Projects
        {
            get
            {
                if (projectRepository == null)
                {
                    projectRepository = new ProjectRepository(_context);
                }
                return projectRepository;
            }
        }
         
        public IRepository<TaskEntity> Tasks
        {
            get
            {
                if (taskRepository == null)
                {
                    taskRepository = new TaskRepository(_context);
                }
                return taskRepository;
            }
        }

        public IRepository<TeamEntity> Teams
        {
            get
            {
                if (teamRepository == null)
                {
                    teamRepository = new TeamRepository(_context);
                }
                return teamRepository;
            }
        }

        public async Task Save()
        {
            await _context.SaveChangesAsync();
        }
    }
}
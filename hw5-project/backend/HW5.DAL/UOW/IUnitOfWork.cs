﻿using HW5.DAL.Entities;
using HW5.DAL.Repository;
using System.Threading.Tasks;

namespace HW5.DAL.UOW
{
    public interface IUnitOfWork
    {
        IRepository<ProjectEntity> Projects { get; }
        IRepository<UserEntity> Users { get; }
        IRepository<TaskEntity> Tasks { get; }
        IRepository<TeamEntity> Teams { get; }
        Task Save();
    }
}

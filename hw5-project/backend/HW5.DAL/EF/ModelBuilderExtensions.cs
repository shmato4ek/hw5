﻿using HW5.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace HW5.DAL.EF
{
    public static class ModelBuilderExtensions
    {
        public static void Configure(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProjectEntity>()
                .Property(project => project.CreatedAt)
                .HasDefaultValue(DateTime.Now);

            modelBuilder.Entity<TaskEntity>()
                .Property(task => task.CreatedAt)
                .HasDefaultValue(DateTime.Now);

            modelBuilder.Entity<TaskEntity>()
                .Property(task => task.State)
                .HasDefaultValue(TaskStates.Finished);

            modelBuilder.Entity<TeamEntity>()
                .Property(team => team.CreatedAt)
                .HasDefaultValue(DateTime.Now);

            modelBuilder.Entity<UserEntity>()
                .Property(user => user.RegisteredAt)
                .HasDefaultValue(DateTime.Now);
        }

        public static void Seed(this ModelBuilder modelBuilder)
        {
            var users = new List<UserEntity>
            {
                new UserEntity { FirstName = "Alaya", LastName = "Andersen", Birthday = new DateTime(2000, 3, 25), Email = "anders78@gmail.com", Id = 1, RegisteredAt = new DateTime(2021, 12, 28), TeamId = 1 },
                new UserEntity { FirstName = "Kuba", LastName = "Kay", Birthday = new DateTime(1992, 7, 4), Email = "kubakey92@gmail.com", Id = 2, RegisteredAt = new DateTime(2021, 12, 22), TeamId = 2 },
                new UserEntity { FirstName = "Jevon", LastName = "Melia", Birthday = new DateTime(2013, 3, 11), Email = "jevon.melia@gmail.com", Id = 3, RegisteredAt = new DateTime(2017, 10, 18), TeamId = 2 },
                new UserEntity { FirstName = "Ida", LastName = "Foster", Birthday = new DateTime(2002, 10, 21), Email = "f0st3rida@gmai.com", Id = 4, RegisteredAt = new DateTime(2019, 1, 20), TeamId = 3 },
                new UserEntity { FirstName = "Cadence", LastName = "Watson", Birthday = new DateTime(2010, 8, 25), Email = "cadence12.com", Id = 5, RegisteredAt = new DateTime(2019, 1, 20), TeamId = 3 }
            };

            var projects = new List<ProjectEntity>
            {
                new ProjectEntity { AuthorId = 1, TeamId = 1, CreatedAt = new DateTime(2021, 10, 11), Deadline = new DateTime(2022, 1, 20), Description = "Platform for listening and saving user`s favourite music", Id = 1, Name = "Music platform" },
                new ProjectEntity { AuthorId = 2, TeamId = 2, CreatedAt = new DateTime(2022, 11, 2), Deadline = new DateTime(2022, 2, 21), Description = "Web-site, where users can know more informayion about our company", Id = 2, Name = "Web-site for company" },
                new ProjectEntity { AuthorId = 3, TeamId = 2, CreatedAt = new DateTime(2021, 9, 21), Deadline = new DateTime(2021, 12, 31), Description = "Creating new campus", Id = 3, Name = "KPI Campus" },
                new ProjectEntity { AuthorId = 4, TeamId = 3, CreatedAt = new DateTime(2021, 7, 12), Deadline = new DateTime(2021, 12, 31), Description = "Site for study controll, based on Moodle", Id = 4, Name = "Moodle-based site" },
                new ProjectEntity { AuthorId = 5, TeamId = 3, CreatedAt = new DateTime(2021, 5, 2), Deadline = new DateTime(2022, 3, 4), Description = "New shop need web-site", Id = 5, Name = "Web-site for new shop" }

            };

            var tasks = new List<TaskEntity>
            {
                new TaskEntity { CreatedAt = new DateTime(2021, 10, 11), Id = 1, Name = "Writing back", Description = "Write back for the platform", PerformerId = 5, ProjectId = 1, State = TaskStates.Started },
                new TaskEntity { CreatedAt = new DateTime(2021, 1, 8), Id = 2, Name = "Create simple web-site for company", Description = "Writing back and front for new web-site", PerformerId = 4, ProjectId = 2, State = TaskStates.Started },
                new TaskEntity { CreatedAt = new DateTime(2022, 1, 3), Id = 3, Name = "Find data", Description = "Get all information about students", FinishedAtDate = new DateTime(2021, 12, 30), PerformerId = 3, ProjectId = 3, State = TaskStates.Finished },
                new TaskEntity { CreatedAt = new DateTime(2021, 10, 22), Id = 4, Name = "Write back", Description = "Write back for campus", FinishedAtDate = new DateTime(2021, 12, 30), PerformerId = 3, ProjectId = 3, State = TaskStates.Canceled },
                new TaskEntity { CreatedAt = new DateTime(2021, 9, 21), Id = 5, Name = "Write front", Description = "Write front for campus", FinishedAtDate = new DateTime(2021, 11, 20), PerformerId = 2, ProjectId = 3, State = TaskStates.Canceled },
                new TaskEntity { CreatedAt = new DateTime(2021, 7, 13), Id = 6, Name = "Create idea", Description = "Think about realization", FinishedAtDate = new DateTime(2021, 12, 30), PerformerId = 2, ProjectId = 4, State = TaskStates.Finished },
                new TaskEntity { CreatedAt = new DateTime(2022, 1, 9), Id = 7, Name = "Talk with customer", Description = "Meet the customer and talk with him about the project", PerformerId = 1, ProjectId = 5, State = TaskStates.Started },
                new TaskEntity { CreatedAt = new DateTime(2021, 5, 2), Id = 8, Name = "Implementing", Description = "Implement customer`s thinks", FinishedAtDate = new DateTime(2022, 1, 5), PerformerId = 1, ProjectId = 5, State = TaskStates.Canceled }
            };

            var teams = new List<TeamEntity>
            {
                new TeamEntity { CreatedAt = new DateTime(2021, 03, 18), Id = 1, Name = "Team A" },
                new TeamEntity { CreatedAt = new DateTime(2021, 03, 19), Id = 2, Name = "Team B" },
                new TeamEntity { CreatedAt = new DateTime(2021, 06, 20), Id = 3, Name = "Team C" }
            };

            modelBuilder.Entity<UserEntity>().HasData(users);
            modelBuilder.Entity<ProjectEntity>().HasData(projects);
            modelBuilder.Entity<TaskEntity>().HasData(tasks);
            modelBuilder.Entity<TeamEntity>().HasData(teams);

        }
    }
}

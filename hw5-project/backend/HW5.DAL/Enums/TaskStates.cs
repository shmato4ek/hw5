﻿namespace HW5.DAL.Entities
{
    public enum TaskStates
    {
        Canceled = 0,
        Started,
        Finished,
        Stopped
    }
}

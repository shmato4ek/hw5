﻿using HW5.DAL.Entities.Abstract;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace HW5.DAL.Entities
{
    [Table("Tasks")]
    public class TaskEntity : BaseEntity
    {
        public int ProjectId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int PerformerId { get; set; }
        public TaskStates State { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? FinishedAtDate { get; set; }
        public ProjectEntity Project { get; set; }
        public UserEntity Performer { get; set; }
    }
}

﻿namespace HW5.DAL.Entities.Abstract
{
    public abstract class BaseEntity
    {
        public int Id { get; set; }
    }
}

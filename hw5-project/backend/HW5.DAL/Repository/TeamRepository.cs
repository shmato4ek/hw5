﻿using HW5.DAL.EF;
using HW5.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HW5.DAL.Repository
{
    public class TeamRepository : IRepository<TeamEntity>
    {
        private ProjectsContext _context;
        public TeamRepository(ProjectsContext context)
        {
            _context = context;
        }
        public async Task<TeamEntity> Create(TeamEntity item)
        {
            await _context.Teams.AddAsync(item);
            return item;
        }

        public async Task Delete(int id)
        {
            var teams = await _context.Teams.ToListAsync();
            var team = teams.Where(team => team.Id == id).FirstOrDefault();
            _context.Teams.Remove(team);
        }

        public async Task<TeamEntity> Get(int id)
        {
            var teams = await _context.Teams.ToListAsync();
            return teams.Where(team => team.Id == id).FirstOrDefault();
        }

        public Task<List<TeamEntity>> GetAll()
        {
            return _context.Teams.ToListAsync();
        }

        public async Task<TeamEntity> Update(TeamEntity item)
        {
            var entity = await _context.Teams.FirstOrDefaultAsync(p => p.Id == item.Id);
            if (entity == null)
            {
                throw new Exception();
            }
            entity.Name = item.Name;
            _context.Teams.Update(entity);
            return item;
        }
    }
}

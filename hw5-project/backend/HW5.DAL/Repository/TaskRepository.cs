﻿using HW5.DAL.EF;
using HW5.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HW5.DAL.Repository
{
    public class TaskRepository : IRepository<TaskEntity>
    {
        private ProjectsContext _context;
        public TaskRepository(ProjectsContext context)
        {
            _context = context;
        }
        public async Task<TaskEntity> Create(TaskEntity item)
        {
            await _context.Tasks.AddAsync(item);
            return item;
        }

        public async Task Delete(int id)
        {
            var tasks = await _context.Tasks.ToListAsync();
            var task = tasks.Where(task => task.Id == id).FirstOrDefault();
            _context.Tasks.Remove(task);
        }

        public async Task<TaskEntity> Get(int id)
        {
            var tasks = await _context.Tasks.ToListAsync();
            return tasks.Where(task => task.Id == id).FirstOrDefault();
        }

        public Task<List<TaskEntity>> GetAll()
        {
            return _context.Tasks.ToListAsync();
        }

        public async Task<TaskEntity> Update(TaskEntity item)
        {
            var entity = await _context.Tasks.FirstOrDefaultAsync(p => p.Id == item.Id);
            if (entity == null)
            {
                throw new Exception();
            }
            entity.State = item.State;
            entity.Name = item.Name;
            entity.Description = item.Description;
            entity.PerformerId = item.PerformerId;
            entity.ProjectId = item.ProjectId;
            _context.Tasks.Update(entity);
            return item;
        }
    }
}
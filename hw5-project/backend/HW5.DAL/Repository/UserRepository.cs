﻿using HW5.DAL.EF;
using HW5.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HW5.DAL.Repository
{
    public class UserRepository : IRepository<UserEntity>
    {
        private ProjectsContext _context;
        public UserRepository(ProjectsContext context)
        {
            _context = context;
        }
        public async Task<UserEntity> Create(UserEntity item)
        {
            await _context.Users.AddAsync(item);
            return item;
        }

        public async Task Delete(int id)
        {
            var users = await _context.Users.ToListAsync();
            var user = users.Where(user => user.Id == id).FirstOrDefault();
            _context.Users.Remove(user);
        }

        public async Task<UserEntity> Get(int id)
        {
            var users = await _context.Users.ToListAsync();
            return users.Where(user => user.Id == id).FirstOrDefault();
        }

        public Task<List<UserEntity>> GetAll()
        {
            return _context.Users.ToListAsync();
        }

        public async Task<UserEntity> Update(UserEntity item)
        {
            var entity = await _context.Users.FirstOrDefaultAsync(p => p.Id == item.Id);
            if (entity == null)
            {
                throw new Exception();
            }
            entity.FirstName = item.FirstName;
            entity.LastName = item.LastName;
            entity.Birthday = item.Birthday;
            entity.Email = item.Email;
            entity.TeamId = item.TeamId;
            _context.Users.Update(entity);
            return item;
        }
    }
}

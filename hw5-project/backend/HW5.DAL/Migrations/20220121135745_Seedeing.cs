﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HW5.DAL.Migrations
{
    public partial class Seedeing : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[] { 1, new DateTime(2021, 3, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), "Team A" });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[] { 2, new DateTime(2021, 3, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), "Team B" });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[] { 3, new DateTime(2021, 6, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "Team C" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Birthday", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 1, new DateTime(2000, 3, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "anders78@gmail.com", "Alaya", "Andersen", new DateTime(2021, 12, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), 1 },
                    { 2, new DateTime(1992, 7, 4, 0, 0, 0, 0, DateTimeKind.Unspecified), "kubakey92@gmail.com", "Kuba", "Kay", new DateTime(2021, 12, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), 2 },
                    { 3, new DateTime(2013, 3, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), "jevon.melia@gmail.com", "Jevon", "Melia", new DateTime(2017, 10, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), 2 },
                    { 4, new DateTime(2002, 10, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), "f0st3rida@gmai.com", "Ida", "Foster", new DateTime(2019, 1, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), 3 },
                    { 5, new DateTime(2010, 8, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "cadence12.com", "Cadence", "Watson", new DateTime(2019, 1, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), 3 }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "DeadLine", "Description", "Name", "TeamId" },
                values: new object[,]
                {
                    { 1, 1, new DateTime(2021, 10, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2022, 1, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "Platform for listening and saving user`s favourite music", "Music platform", 1 },
                    { 2, 2, new DateTime(2022, 11, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2022, 2, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), "Web-site, where users can know more informayion about our company", "Web-site for company", 2 },
                    { 3, 3, new DateTime(2021, 9, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2021, 12, 31, 0, 0, 0, 0, DateTimeKind.Unspecified), "Creating new campus", "KPI Campus", 2 },
                    { 4, 4, new DateTime(2021, 7, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2021, 12, 31, 0, 0, 0, 0, DateTimeKind.Unspecified), "Site for study controll, based on Moodle", "Moodle-based site", 3 },
                    { 5, 5, new DateTime(2021, 5, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2022, 3, 4, 0, 0, 0, 0, DateTimeKind.Unspecified), "New shop need web-site", "Web-site for new shop", 3 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 1, new DateTime(2021, 10, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), "Write back for the platform", null, "Writing back", 5, 1, 1 },
                    { 2, new DateTime(2021, 1, 8, 0, 0, 0, 0, DateTimeKind.Unspecified), "Writing back and front for new web-site", null, "Create simple web-site for company", 4, 2, 1 },
                    { 3, new DateTime(2022, 1, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "Get all information about students", new DateTime(2021, 12, 30, 0, 0, 0, 0, DateTimeKind.Unspecified), "Find data", 3, 3, 2 },
                    { 4, new DateTime(2021, 10, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), "Write back for campus", new DateTime(2021, 12, 30, 0, 0, 0, 0, DateTimeKind.Unspecified), "Write back", 3, 3, 0 },
                    { 5, new DateTime(2021, 9, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), "Write front for campus", new DateTime(2021, 11, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "Write front", 2, 3, 0 },
                    { 6, new DateTime(2021, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), "Think about realization", new DateTime(2021, 12, 30, 0, 0, 0, 0, DateTimeKind.Unspecified), "Create idea", 2, 4, 2 },
                    { 7, new DateTime(2022, 1, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), "Meet the customer and talk with him about the project", null, "Talk with customer", 1, 5, 1 },
                    { 8, new DateTime(2021, 5, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), "Implement customer`s thinks", new DateTime(2022, 1, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Implementing", 1, 5, 0 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3);
        }
    }
}

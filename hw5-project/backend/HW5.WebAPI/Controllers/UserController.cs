﻿using Common.DTO;
using Common.DTO.User;
using HW5.BLL.Interfaces;
using HW5.BLL.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HW5.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IService<UserDTO, UserCreatedDTO> _userService;

        public UserController(IService<UserDTO, UserCreatedDTO> userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public async Task<ActionResult<List<UserDTO>>> Get()
        {
            var users = await _userService.GetAll();
            return Ok(users);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<UserDTO>> GetById(int id)
        {
            try
            {
                var result = await _userService.Get(id);
                return Ok(result);
            }
            catch(Exception)
            {
                throw new Exception($"User with id {id} was not found");
            }
        }

        [HttpPost]
        public async Task<ActionResult<UserDTO>> CreateUser([FromBody] UserCreatedDTO user)
        {
            try
            {
                var newUser = await _userService.Add(user);
                return Created($"~api/user/{newUser.Id}", newUser);
            }
            catch(Exception)
            {
                throw new Exception("This structure of user is uncorrect");
            }
        }

        [HttpPut]
        public async Task<ActionResult<UserDTO>> UpdateUser([FromBody] UserDTO user)
        {
            try
            {
                var updatedUser = await _userService.Update(user);
                return Created($"~api/team/{updatedUser.Id}", updatedUser);
            }
            catch(Exception)
            {
                throw new Exception("This structure of user is uncorrect");
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteUser(int id)
        {
            try
            {
                await _userService.Delete(id);
                return NoContent();
            }
            catch(Exception)
            {
                throw new Exception($"User with id {id} was not found");
            }
        }
    }
}

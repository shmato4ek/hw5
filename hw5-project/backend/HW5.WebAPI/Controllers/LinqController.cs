﻿using Common.DTO;
using HW5.BLL.Services;
using HW5.BLL.Structs;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HW2.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LinqController : ControllerBase
    {
        private readonly LinqService _linqService;
        public LinqController(LinqService linqService)
        {
            _linqService = linqService;
        }

        [HttpGet("user/{id}/tasks/amount")]
        public async Task<ActionResult<List<ProjectDictionaryDTO>>> GetAmmountOfTasksOfConcreteUser(int id)
        {
            try
            {
                var result = await _linqService.GetAmmountOfTasksOfConcreteUser(id);
                return Ok(result);
            }
            catch (Exception)
            {
                throw new Exception($"User with id {id} was not found");
            }
        }

        [HttpGet("user/{id}/tasks")]
        public async Task<ActionResult<List<TaskDTO>>> GetAllTasksOfConcreteUser(int id)
        {
            try
            {
                var result = await _linqService.GetAllTasksOfConcreteUser(id);
                return Ok(result);
            }
            catch(Exception)
            {
                throw new Exception($"User with id {id} was not found");
            }
        }

        [HttpGet("user/{id}/tasks/finished")]
        public async Task<ActionResult<List<TaskDictionaryDTO>>> GetFinishedTasksOfConcreteUser(int id)
        {
            try
            {
                var result = await _linqService.GetFinishedTasksOfConcreteUser(id);
                return Ok(result);
            }
            catch(Exception)
            {
                throw new Exception($"User with id {id} was not found");
            }
        }

        [HttpGet("teams/older-then10")]
        public async Task<ActionResult<List<TeamWithUsersStruct>>> GetTeamsWithUsersOlderThan10()
        {
            var result = await _linqService.GetTeamsWithUsersOlderThan10();
            return Ok(result);
        }

        [HttpGet("users/sorted")]
        public async Task<ActionResult<List<UserDictionaryDTO>>> GetSortedListOfUsers()
        {
            var result = await _linqService.GetSortedListOfUsers();
            return Ok(result);
        }

        [HttpGet("user/{id}/info")]
        public async Task<ActionResult<UserStructDTO>> GetUserStruct(int id)
        {
            try
            {
                var result = await _linqService.GetUserStruct(id);
                return Ok(result);
            }
            catch(Exception)
            {
                throw new Exception($"User with id {id} was not found");
            }
        }

        [HttpGet("projects/info")]
        public async Task<ActionResult<List<ProjectStruct>>> GetProjectStruct()
        {
            var result = await _linqService.GetProjectStruct();
            return Ok(result);
        }
    }
}
    
﻿using Common.DTO;
using Common.DTO.Task;
using HW5.BLL.Interfaces;
using HW5.BLL.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HW5.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly IService<TaskDTO, TaskCreatedDTO> _taskService;

        public TaskController(IService<TaskDTO, TaskCreatedDTO> taskService)
        {
            _taskService = taskService;
        }

        [HttpGet]
        public async Task<ActionResult<List<TaskDTO>>> Get()
        {
            var result = await _taskService.GetAll();
            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TaskDTO>> GetById(int id)
        {
            try
            {
                var result = await _taskService.Get(id);
                if(result is null)
                {
                    throw new Exception();
                }
                return Ok(result);
            }
            catch(Exception)
            {
                throw new Exception($"Task with id {id} was not found");
            }
        }

        [HttpPost]
        public async Task<ActionResult<TaskDTO>> CreateTask([FromBody] TaskCreatedDTO task)
        {
            try
            {
                var newTask = await _taskService.Add(task);
                return Created($"~api/task/{newTask.Id}", newTask);
            }
            catch(Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        [HttpPut]
        public async Task<ActionResult<TaskDTO>> UpdateTask([FromBody] TaskDTO task)
        {
            try
            {
                var updatedTask = await _taskService.Update(task);
                return Created($"~api/task/{updatedTask.Id}", updatedTask);
            }
            catch(Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteTask(int id)
        {
            try
            {
                await _taskService.Delete(id);
                return NoContent();
            }
            catch
            {
                throw new Exception($"Task with id {id} was not found");
            }
        }
    }
}

import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { TaskDictionary } from '../../task/models/taskDictionary';
import { ProjectDictionary } from '../../project/models/projectDictionary'; 
import { Task } from '../../task/models/task';
import { TeamWithUsersStruct } from '../../team/models/teamWithUsersStruct';
import { UserDictionary } from '../../user/models/userDictionary';
import { UserStruct } from '../../user/models/userStruct';
import { ProjectStruct } from '../../project/models/projectStruct';

@Injectable({ providedIn: 'root' })
export class LinqService {
  public routePrefix = '/api/linq';
  constructor(private httpService: HttpInternalService) {}

  public GetAmountOfTasksOfConcreteUser(id: number) {
    return this.httpService.getFullRequest<ProjectDictionary[]>(
      `${this.routePrefix}/user/${id}/tasks/amount`
    );
  }

  public GetAllTasksOfConcreteUser(id: number) {
    return this.httpService.getFullRequest<Task[]>(
      `${this.routePrefix}/user/${id}/tasks`
    );
  }

  public GetFinishedTasksOfConcreteUser(id: number) {
    return this.httpService.getFullRequest<TaskDictionary[]>(
      `${this.routePrefix}/user/${id}/tasks/finished`
    );
  }

  public GetTeamsWithUsersOlderThan10() {
    return this.httpService.getFullRequest<TeamWithUsersStruct[]>(
      `${this.routePrefix}/teams/older-then10`
    );
  }

  public GetSortedListOfUsers() {
    return this.httpService.getFullRequest<UserDictionary[]>(
      `${this.routePrefix}/users/sorted`
    );
  }

  public GetUserStruct(id: number) {
    return this.httpService.getFullRequest<UserStruct>(
      `${this.routePrefix}/user/${id}/info`
    );
  }

  public GetProjectStruct() {
    return this.httpService.getFullRequest<ProjectStruct>(
      `${this.routePrefix}/projects/info`
    );
  }
}

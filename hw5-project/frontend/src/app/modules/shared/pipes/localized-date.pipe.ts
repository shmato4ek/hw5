import { PipeTransform, Pipe } from '@angular/core';

@Pipe({
    name: 'localizedDate',
    pure: false
  })
  export class LocalizedDatePipe implements PipeTransform {

      transform(value: Date): string {
        const options = {day: 'numeric', month: 'long', year: 'numeric' } as const;
        const [day, month, year] = (new Date(value)).toLocaleDateString('uk', options).split(' ')
        return [day, month, year].join(' ');
    }
}
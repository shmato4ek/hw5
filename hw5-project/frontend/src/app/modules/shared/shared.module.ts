import { NgModule } from '@angular/core';
import { MaterialComponentsModule } from './components/common/material-components.module';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { ItemComponent } from './components/item/item.component';
import { CommonModule } from '@angular/common';
import { CanCloseGuard } from './guards/can-close.guard';
import { LocalizedDatePipe } from './pipes/localized-date.pipe';
import { DateAdapter, MAT_DATE_LOCALE } from '@angular/material/core';
import {
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
  MomentDateAdapter,
} from '@angular/material-moment-adapter';

@NgModule({
  declarations: [SidebarComponent, ItemComponent, LocalizedDatePipe],
  imports: [MaterialComponentsModule, CommonModule],
  exports: [
    MaterialComponentsModule,
    SidebarComponent,
    ItemComponent,
    LocalizedDatePipe,
  ],
  providers: [CanCloseGuard, { provide: MAT_DATE_LOCALE, useValue: 'en-GB' }],
})
export class SharedModule {}

import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { CustomBoolean } from '../types/custom-boolean.type';

export type CanCloseComponent = {
  canClose: () => CustomBoolean;
};

@Injectable()
export class CanCloseGuard implements CanDeactivate<CanCloseComponent> {
  canDeactivate(component: CanCloseComponent): CustomBoolean {
    return (
      component.canClose() ||
      confirm(
        'Save changes before leave the page or push OK to cancel all changes'
      )
    );
  }
}

import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css'],
})
export class ItemComponent {
  @Input() public title: string;
  @Input() public id: number;
  @Input() public onDelete: (id: number) => void;
  @Input() public onChange: (id: number) => void;
  public isItemExtended = false;

  toggleDetails() {
    this.isItemExtended = !this.isItemExtended;
  }

  handleChange() {
    this.onChange(this.id);
  }

  handleDelete() {
    this.onDelete(this.id);
  }
}

import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { TeamsListComponent } from './components/team-list/team-list.component';
import { BrowserModule } from '@angular/platform-browser';
import { ChangeTeamComponent } from './components/change-team/change-team.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';

@NgModule({
  declarations: [TeamsListComponent, ChangeTeamComponent],
  imports: [
    SharedModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
  ],
  exports: [TeamsListComponent],
})
export class TeamModule {}

import { User } from "../../user/models/user";

export interface TeamWithUsersStruct
{
  id: number;
  teamName: string;
  users: User[];
}

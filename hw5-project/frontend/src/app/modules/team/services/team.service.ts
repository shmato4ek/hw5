import { Injectable } from '@angular/core';
import { HttpInternalService } from '../../shared/services/http-internal.service';
import { CreatedTeam } from '../models/createdTeam';
import { Team } from '../models/team';

@Injectable({ providedIn: 'root' })
export class TeamService {
  public routePrefix = '/api/team';
  constructor(private httpService: HttpInternalService) {}
  public getTeamById(id: number) {
    return this.httpService.getFullRequest<Team>(`${this.routePrefix}/${id}`);
  }
  public getAllTeams() {
    return this.httpService.getFullRequest<Team[]>(`${this.routePrefix}`);
  }

  public AddTeam(newTeam: CreatedTeam) {
    return this.httpService.postFullRequest<Team>(
      `${this.routePrefix}`,
      newTeam
    );
  }

  public UpdateTeam(team: Team) {
    return this.httpService.putFullRequest<void>(this.routePrefix, team);
  }

  public DeleteTeam(id: number) {
    return this.httpService.deleteFullRequest<void>(
      `${this.routePrefix}/${id}`
    );
  }
}

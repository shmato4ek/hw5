import { Component, OnInit } from '@angular/core';
import { TeamService } from 'src/app/modules/team/services/team.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Team } from '../../models/team';
import { Router } from '@angular/router';
import { SnackBarMessage } from 'src/app/enums/snackbar-message.enum';
import { SnackBarService } from 'src/app/modules/shared/services/snack-bar.service';

@Component({
  selector: 'app-team-list',
  templateUrl: './team-list.component.html',
  styleUrls: ['./team-list.component.css'],
})
export class TeamsListComponent implements OnInit {
  public teams: Team[];
  public constructor(
    private teamService: TeamService,
    private snackBarService: SnackBarService,
    private router: Router
  ) {}
  private unsubscribe$ = new Subject<void>();

  public ngOnInit() {
    this.onDelete = this.onDelete.bind(this);
    this.onChange = this.onChange.bind(this);
    this.teamService
      .getAllTeams()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: (response: any) => {
          this.teams = response.body;
        },
        error: (error) => {
          this.snackBarService.showErrorMessage(error);
        },
      });
  }

  public onDelete(id: number) {
    const isDeletingConfirmed = confirm('Are you sure to delete it?');
    if (isDeletingConfirmed) {
      this.teamService
        .DeleteTeam(id)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe({
          next: (response: any) => {
            this.teams = this.teams.filter((team) => team.id !== id);
            this.snackBarService.showSuccessMessage(
              SnackBarMessage.SUCCESSFULLY_DELETED
            );
          },
          error: (error) => {
            this.snackBarService.showErrorMessage(error);
          },
        });
    }
  }

  public onChange(id: number) {
    this.router.navigate([`/team/${id}/change`]);
  }

  public onCreate() {
    this.router.navigate([`/team/create`]);
  }
}

import { Component, Input, OnInit } from '@angular/core';
import { TeamService } from '../../services/team.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Team } from '../../models/team';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UserService } from 'src/app/modules/user/services/user.service';
import { User } from 'src/app/modules/user/models/user';
import { MIN_NAME_LENGTH } from '../../constants/validation.constants';
import { CustomBoolean } from 'src/app/types/custom-boolean.type';
import { SnackBarService } from 'src/app/modules/shared/services/snack-bar.service';
import { SnackBarMessage } from 'src/app/enums/snackbar-message.enum';

@Component({
  selector: 'app-change-team',
  templateUrl: './change-team.component.html',
  styleUrls: ['./change-team.component.css'],
})
export class ChangeTeamComponent implements OnInit {
  public team = {} as Team;
  public users: User[];
  public isEditing: boolean;
  public teamId: number;
  public teamForm: FormGroup;
  private isChangesSaved: boolean;
  private unsubscribe$ = new Subject<void>();
  public constructor(
    private teamService: TeamService,
    activateRoute: ActivatedRoute,
    private snackBarService: SnackBarService,
    private userService: UserService,
    private router: Router
  ) {
    this.isChangesSaved = false;
    this.teamId = activateRoute.snapshot.params['id'];
    this.isEditing = !!this.teamId;
  }

  public canClose(): CustomBoolean {
    return !this.teamForm.dirty || this.isChangesSaved;
  }

  ngOnInit(): void {
    if (this.teamId) {
      this.teamService
        .getTeamById(this.teamId)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe({
          next: (response: any) => {
            this.team = response.body;
          },
          error: (error) => {
            this.snackBarService.showErrorMessage(error);
          },
        });
    }
    this.userService
      .getAllUsers()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: (response: any) => {
          this.users = response.body;
        },
        error: (error) => {
          this.snackBarService.showErrorMessage(error);
        },
      });

    this.teamForm = new FormGroup({
      name: new FormControl(this.team.name, [
        Validators.required,
        Validators.minLength(MIN_NAME_LENGTH),
      ]),
    });
  }

  updateTeam() {
    this.teamService
      .UpdateTeam(this.team)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: (response: any) => {
          this.team = response.body;
          this.isChangesSaved = true;
          this.router.navigate([`/task`]);
          this.snackBarService.showSuccessMessage(
            SnackBarMessage.SUCCESSFULLY_UPDATED
          );
        },
        error: (error) => {
          this.snackBarService.showErrorMessage(error);
        },
      });
  }
  createTeam() {
    const { name } = this.team;
    this.teamService
      .AddTeam({ name })
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: (response: any) => {
          this.team = response.body;
          this.isChangesSaved = true;
          this.router.navigate([`/team`]);
          this.snackBarService.showSuccessMessage(
            SnackBarMessage.SUCCESSFULLY_CREATED
          );
        },
        error: (error) => {
          this.snackBarService.showErrorMessage(error);
        },
      });
  }
  public handleCancel() {
    this.router.navigate([`/team`]);
  }
}

import { Component, OnInit } from '@angular/core';
import { ProjectService } from '../../services/project.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Project } from '../../models/project';
import { Router } from '@angular/router';
import { User } from 'src/app/modules/user/models/user';
import { TeamModule } from 'src/app/modules/team/team.module';
import { Team } from 'src/app/modules/team/models/team';
import { TeamService } from 'src/app/modules/team/services/team.service';
import { UserService } from 'src/app/modules/user/services/user.service';
import { SnackBarService } from 'src/app/modules/shared/services/snack-bar.service';
import { SnackBarMessage } from 'src/app/enums/snackbar-message.enum';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css'],
})
export class ProjectsListComponent implements OnInit {
  public projects: Project[];
  public users: User[];
  public teams: Team[];
  public constructor(
    private projectService: ProjectService,
    private teamService: TeamService,
    private userService: UserService,
    private snackBarService: SnackBarService,
    private router: Router
  ) {}
  private unsubscribe$ = new Subject<void>();

  public ngOnInit() {
    this.onDelete = this.onDelete.bind(this);
    this.onChange = this.onChange.bind(this);
    this.projectService
      .getAllProjects()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: (response: any) => {
          this.projects = response.body;
        },
        error: (error) => {
          this.snackBarService.showErrorMessage(error);
        },
      });
    this.userService
      .getAllUsers()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: (response: any) => {
          this.users = response.body;
        },
        error: (error) => {
          this.snackBarService.showErrorMessage(error);
        },
      });
    this.teamService
      .getAllTeams()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: (response: any) => {
          this.teams = response.body;
        },
        error: (error) => {
          this.snackBarService.showErrorMessage(error);
        },
      });
  }

  public onDelete(id: number) {
    const isDeletingConfirmed = confirm('Are you sure to delete it?');
    if (isDeletingConfirmed) {
      this.projectService
        .DeleteProject(id)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe({
          next: (response: any) => {
            this.projects = this.projects.filter(
              (project) => project.id !== id
            );
            this.snackBarService.showSuccessMessage(SnackBarMessage.SUCCESSFULLY_DELETED);
          },
          error: (error) => {
            this.snackBarService.showErrorMessage(error);
          },
        });
    }
  }

  public onChange(id: number) {
    this.router.navigate([`/project/${id}/change`]);
  }

  public onCreate() {
    this.router.navigate([`/project/create`]);
  }

  public getAuthorName(authorId: number) {
    const author = this.users.find((user) => user.id == authorId);
    return `${author.firstName} ${author.lastName}`;
  }

  public getTeamName(teamId: number) {
    return this.teams.find((team) => team.id == teamId).name;
  }
}

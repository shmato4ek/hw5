import { Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Project } from 'src/app/modules/project/models/project';
import { ProjectService } from 'src/app/modules/project/services/project.service';
import { MIN_NAME_LENGTH } from '../../constants/validation.constants';
import { CustomBoolean } from '../../../../types/custom-boolean.type';
import { UserService } from 'src/app/modules/user/services/user.service';
import { User } from 'src/app/modules/user/models/user';
import { Team } from 'src/app/modules/team/models/team';
import { TeamService } from 'src/app/modules/team/services/team.service';
import { SnackBarService } from 'src/app/modules/shared/services/snack-bar.service';
import { SnackBarMessage } from 'src/app/enums/snackbar-message.enum';

@Component({
  selector: 'app-change-project',
  templateUrl: './change-project.component.html',
  styleUrls: ['./change-project.component.css'],
})
export class ChangeProjectComponent implements OnInit {
  public project = {} as Project;
  public users: User[];
  public teams: Team[];
  public isEditing: boolean;
  public projectId: number;
  public projectForm: FormGroup;
  private isChangesSaved: boolean;
  private unsubscribe$ = new Subject<void>();
  public constructor(
    activateRoute: ActivatedRoute,
    private projectService: ProjectService,
    private userService: UserService,
    private teamService: TeamService,
    private snackBarService: SnackBarService,
    private router: Router
  ) {
    this.isChangesSaved = false;
    this.projectId = activateRoute.snapshot.params['id'];
    this.isEditing = !!this.projectId;
  }

  public canClose(): CustomBoolean {
    return !this.projectForm.dirty || this.isChangesSaved;
  }

  ngOnInit(): void {
    if (this.projectId) {
      this.projectService
        .getProjectById(this.projectId)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe({
          next: (response: any) => {
            this.project = response.body;
          },
          error: (error) => {
            this.snackBarService.showErrorMessage(error);
          },
        });
    }
    this.userService
      .getAllUsers()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: (response: any) => {
          this.users = response.body;
        },
        error: (error) => {
          this.snackBarService.showErrorMessage(error);
        },
      });
    this.teamService
      .getAllTeams()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: (response: any) => {
          this.teams = response.body;
        },
        error: (error) => {
          this.snackBarService.showErrorMessage(error);
        },
      });

    this.projectForm = new FormGroup({
      name: new FormControl(this.project.name, [
        Validators.required,
        Validators.minLength(MIN_NAME_LENGTH),
      ]),
      author: new FormControl(this.users, Validators.required),
      description: new FormControl(
        this.project.description,
        Validators.required
      ),
      team: new FormControl(this.teams, Validators.required),
      deadline: new FormControl(this.project.deadline, Validators.required),
    });
  }

  updateProject() {
    const localizedDeadline = new Date(this.project.deadline);
    localizedDeadline.setMinutes(
      localizedDeadline.getMinutes() - localizedDeadline.getTimezoneOffset()
    );
    this.projectService
      .UpdateProject({ ...this.project, deadline: localizedDeadline })
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: (response: any) => {
          this.project = response.body;
          this.project.deadline;
          this.isChangesSaved = true;
          this.router.navigate([`/project`]);
          this.snackBarService.showSuccessMessage(SnackBarMessage.SUCCESSFULLY_UPDATED);
        },
        error: (error) => {
          this.snackBarService.showErrorMessage(error);
        },
      });
  }
  createProject() {
    const { description, name, deadline, authorId, teamId } = this.project;
    const localizedDeadline = new Date(deadline);
    localizedDeadline.setMinutes(
      localizedDeadline.getMinutes() - localizedDeadline.getTimezoneOffset()
    );
    this.projectService
      .AddProject({
        description,
        name,
        deadline: localizedDeadline,
        authorId,
        teamId,
      })
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: (response: any) => {
          this.project = response.body;
          this.isChangesSaved = true;
          this.router.navigate([`/project`]);
          this.snackBarService.showSuccessMessage(SnackBarMessage.SUCCESSFULLY_CREATED);
        },
        error: (error) => {
          this.snackBarService.showErrorMessage(error);
        },
      });
  }
  public handleCancel() {
    this.router.navigate([`/project`]);
  }
}

import { Project } from "./project";

export interface ProjectDictionary
{
  Key: Project;
  Value: number;
}

import { Task } from "../../task/models/task";
import { Project } from "./project";

export interface ProjectStruct
{
  project: Project;
  theLongestTask: Task;
  theShortestTask: Task;
  ammountOfUsers: number;
}

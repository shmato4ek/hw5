export interface CreatedProject {
  authorId: number;
  teamId: number;
  name: string;
  description: string;
  deadline: Date;
}

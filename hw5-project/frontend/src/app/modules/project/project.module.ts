import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { ProjectsListComponent } from './components/project-list/project-list.component';
import { BrowserModule } from '@angular/platform-browser';
import { ChangeProjectComponent } from './components/change-project/change-project.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';

@NgModule({
  declarations: [ProjectsListComponent, ChangeProjectComponent],
  imports: [
    SharedModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
  ],
  exports: [ProjectsListComponent],
})
export class ProjectModule {}

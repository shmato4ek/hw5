import { Injectable } from '@angular/core';
import { HttpInternalService } from '../../shared/services/http-internal.service';
import { CreatedProject } from '../models/CreatedProject';
import { Project } from '../models/project';

@Injectable({ providedIn: 'root' })
export class ProjectService {
  public routePrefix = '/api/project';
  constructor(private httpService: HttpInternalService) {}
  public getProjectById(id: number) {
    return this.httpService.getFullRequest<Project>(
      `${this.routePrefix}/${id}`
    );
  }
  public getAllProjects() {
    return this.httpService.getFullRequest<Project[]>(`${this.routePrefix}`);
  }

  public AddProject(newProject: CreatedProject) {
    return this.httpService.postFullRequest<Project>(
      `${this.routePrefix}`,
      newProject
    );
  }

  public UpdateProject(project: Project) {
    return this.httpService.putFullRequest<void>(this.routePrefix, project);
  }

  public DeleteProject(id: number) {
    return this.httpService.deleteFullRequest<void>(
      `${this.routePrefix}/${id}`
    );
  }
}

import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { User } from '../../models/user';
import { Router } from '@angular/router';
import { SnackBarMessage } from 'src/app/enums/snackbar-message.enum';
import { SnackBarService } from 'src/app/modules/shared/services/snack-bar.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css'],
})
export class UsersListComponent implements OnInit {
  public users: User[];
  public fullName: string;
  public constructor(
    private userService: UserService,
    private snackBarService: SnackBarService,
    private router: Router
  ) {}
  private unsubscribe$ = new Subject<void>();

  public ngOnInit() {
    this.onDelete = this.onDelete.bind(this);
    this.onChange = this.onChange.bind(this);
    this.userService
      .getAllUsers()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: (response: any) => {
          this.users = response.body;
        },
        error: (error) => {
          this.snackBarService.showErrorMessage(error);
        },
      });
  }

  public onDelete(id: number) {
    const isDeletingConfirmed = confirm(
      'Are you sure to delete it?'
    );
    if (isDeletingConfirmed) {
      this.userService
        .DeleteUser(id)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe({
          next: (response: any) => {
            this.users = this.users.filter(
              (user) => user.id !== id
            );
            this.snackBarService.showSuccessMessage(SnackBarMessage.SUCCESSFULLY_DELETED);
          },
          error: (error) => {
            this.snackBarService.showErrorMessage(error);
          },
        });
    }
  }

  public onChange(id: number) {
    this.router.navigate([`/user/${id}/change`]);
  }

  public onCreate() {
    this.router.navigate([`/user/create`]);
  }
}

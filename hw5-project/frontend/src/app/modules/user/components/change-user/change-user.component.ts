import { Component, Input, OnInit } from '@angular/core';
import { UserService } from 'src/app/modules/user/services/user.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/modules/user/models/user';
import { MIN_NAME_LENGTH } from '../../constants/validation.constants';
import { CustomBoolean } from 'src/app/types/custom-boolean.type';
import { TeamService } from 'src/app/modules/team/services/team.service';
import { Team } from 'src/app/modules/team/models/team';
import { SnackBarService } from 'src/app/modules/shared/services/snack-bar.service';
import { SnackBarMessage } from 'src/app/enums/snackbar-message.enum';

@Component({
  selector: 'app-change-user',
  templateUrl: './change-user.component.html',
  styleUrls: ['./change-user.component.css'],
})
export class ChangeUserComponent implements OnInit {
  public user = {} as User;
  public users: User[];
  public teams: Team[];
  public isEditing: boolean;
  public userId: number;
  public userForm: FormGroup;
  private isChangesSaved: boolean;
  private unsubscribe$ = new Subject<void>();
  public constructor(
    activateRoute: ActivatedRoute,
    private userService: UserService,
    private snackBarService: SnackBarService,
    private teamService: TeamService,
    private router: Router
  ) {
    this.isChangesSaved = false;
    this.userId = activateRoute.snapshot.params['id'];
    this.isEditing = !!this.userId;
  }

  public canClose(): CustomBoolean {
    return !this.userForm.dirty || this.isChangesSaved;
  }

  ngOnInit(): void {
    if (this.userId) {
      this.userService
        .getUserById(this.userId)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe({
          next: (response: any) => {
            this.user = response.body;
          },
          error: (error) => {
            this.snackBarService.showErrorMessage(error);
          },
        });
    }
    this.userService
      .getAllUsers()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: (response: any) => {
          this.users = response.body;
        },
        error: (error) => {
          this.snackBarService.showErrorMessage(error);
        },
      });

    this.teamService
      .getAllTeams()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: (response: any) => {
          this.teams = response.body;
        },
        error: (error) => {
          this.snackBarService.showErrorMessage(error);
        },
      });

    this.userForm = new FormGroup({
      firstName: new FormControl(this.user.firstName, [
        Validators.required,
        Validators.minLength(MIN_NAME_LENGTH),
      ]),
      lastName: new FormControl(this.user.lastName, [
        Validators.required,
        Validators.minLength(MIN_NAME_LENGTH),
      ]),
      birthday: new FormControl(this.user.birthday, [Validators.required]),
      email: new FormControl(this.user.email, [Validators.required]),
      team: new FormControl(this.teams, [Validators.required]),
    });
  }

  updateUser() {
    const localizedBirthday = new Date(this.user.birthday);
    localizedBirthday.setMinutes(
      localizedBirthday.getMinutes() - localizedBirthday.getTimezoneOffset()
    );
    this.userService
      .UpdateUser({ ...this.user, birthday: localizedBirthday })
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: (response: any) => {
          this.user = response.body;
          this.isChangesSaved = true;
          this.router.navigate([`/task`]);
          this.snackBarService.showSuccessMessage(
            SnackBarMessage.SUCCESSFULLY_UPDATED
          );
        },
        error: (error) => {
          this.snackBarService.showErrorMessage(error);
        },
      });
  }
  createUser() {
    const { firstName, lastName, birthday, email, teamId } = this.user;
    const localizedBirthday = new Date(birthday);
    localizedBirthday.setMinutes(
      localizedBirthday.getMinutes() - localizedBirthday.getTimezoneOffset()
    );
    this.userService
      .AddUser({
        firstName,
        lastName,
        birthday: localizedBirthday,
        email,
        teamId,
      })
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: (response: any) => {
          this.user = response.body;
          this.isChangesSaved = true;
          this.router.navigate([`/user`]);
          this.snackBarService.showSuccessMessage(
            SnackBarMessage.SUCCESSFULLY_CREATED
          );
        },
        error: (error) => {
          this.snackBarService.showErrorMessage(error);
        },
      });
  }
}

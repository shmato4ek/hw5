import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { UsersListComponent } from './components/user-list/user-list.component';
import { BrowserModule } from '@angular/platform-browser';
import { ChangeUserComponent } from './components/change-user/change-user.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';

@NgModule({
  declarations: [UsersListComponent, ChangeUserComponent],
  imports: [
    SharedModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
  ],
  exports: [UsersListComponent],
})
export class UserModule {}

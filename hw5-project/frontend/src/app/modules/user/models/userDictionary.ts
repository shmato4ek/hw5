import { User } from "./user";
import { Task } from "../../modules/task/models/task";

export interface UserDictionary
{
  key: User;
  value: Task[];
}

import { User } from "./user";
import { Project } from "../../project/models/project";
import { Task } from "../../task/models/task";

export interface UserStruct
{
  user: User;
  lastProject: Project;
  amountOfTaskInLastProject: number;
  amountOfNotFinishedOrCanceledTasks: number;
  theLongestTask: Task;
}

export interface CreatedUser {
  firstName: string;
  lastName: string;
  teamId: number;
  email: string;
  birthday: Date;
}

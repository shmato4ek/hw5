import { Injectable } from '@angular/core';
import { HttpInternalService } from '../../shared/services/http-internal.service';
import { CreatedUser } from '../models/createdUser';
import { User } from '../models/user';

@Injectable({ providedIn: 'root' })
export class UserService {
  public routePrefix = '/api/user';
  constructor(private httpService: HttpInternalService) {}
  public getUserById(id: number) {
    return this.httpService.getFullRequest<User>(`${this.routePrefix}/${id}`);
  }
  public getAllUsers() {
    return this.httpService.getFullRequest<User[]>(`${this.routePrefix}`);
  }

  public AddUser(newUser: CreatedUser) {
    return this.httpService.postFullRequest<CreatedUser>(
      `${this.routePrefix}`,
      newUser
    );
  }

  public UpdateUser(user: User) {
    return this.httpService.putFullRequest<void>(this.routePrefix, user);
  }

  public DeleteUser(id: number) {
    return this.httpService.deleteFullRequest<void>(
      `${this.routePrefix}/${id}`
    );
  }
}

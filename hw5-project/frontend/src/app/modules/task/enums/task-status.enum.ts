export enum TaskStatus {
  CANCELED = 0,
  STARTED = 1,
  FINISHED = 2,
  STOPPED = 3,
}

import {Directive, ElementRef, Input, OnInit, SimpleChanges} from '@angular/core';
import { TaskStatus } from '../enums/task-status.enum';

@Directive({
  selector: '[appTaskState]'
})
export class TaskStateDirective implements OnInit{
  @Input() status: TaskStatus = {} as TaskStatus;

  constructor(private el: ElementRef) { }

  ngOnInit(): void {
    switch(this.status){
      case TaskStatus.CANCELED : {
        this.el.nativeElement.style.color = "#FF0000";
        this.el.nativeElement.innerText = "Canceled";
        break;
      }
      case TaskStatus.FINISHED : {
        this.el.nativeElement.style.color = "#00FF00";
        this.el.nativeElement.innerText = "Finished";
        break;
      }
      case TaskStatus.STARTED : {
        this.el.nativeElement.style.color = "#FFFF00";
        this.el.nativeElement.innerText = "Started";
        break;
      }
      case TaskStatus.STOPPED : {
        this.el.nativeElement.style.color = "#FF4500";
        this.el.nativeElement.innerText = "Stopped";
        break;
      }
      
    }
  }
}
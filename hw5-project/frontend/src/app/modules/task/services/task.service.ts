import { Injectable } from "@angular/core";
import { HttpInternalService } from "../../shared/services/http-internal.service";
import { CreatedTask } from "../models/createdTask";
import { Task } from "../models/task";

@Injectable({ providedIn: "root" })
export class TaskService
{
  public routePrefix = "/api/task";
  constructor(private httpService: HttpInternalService) {}
  public getTaskById(id: number)
  {
    return this.httpService.getFullRequest<Task>(`${this.routePrefix}/${id}`);
  }
  public getAllTasks()
  {
    return this.httpService.getFullRequest<Task[]>(`${this.routePrefix}`);
  }

  public AddTask(newTask: CreatedTask)
  {
    return this.httpService.postFullRequest<CreatedTask>(
      `${this.routePrefix}`,
      newTask
    );
  }

  public UpdateTask(task: Task)
  {
    return this.httpService.putFullRequest<void>(this.routePrefix, task);
  }

  public DeleteTask(id: number)
  {
    return this.httpService.deleteFullRequest<void>(
      `${this.routePrefix}/${id}`
    );
  }
}

export interface CreatedTask {
  projectId: number;
  performerId: number;
  name: string;
  description: string;
}

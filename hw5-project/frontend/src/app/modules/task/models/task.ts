import { Project } from "../../project/models/project";
import { User } from "../../user/models/user";
import { TaskStatus } from "../enums/task-status.enum";

export interface Task {
  id: number;
  projectId: number;
  performerId: number;
  name: string;
  description: string;
  state: TaskStatus;
  createdAt: Date;
}

export interface TaskDictionary {
  key: number;
  value: string;
}

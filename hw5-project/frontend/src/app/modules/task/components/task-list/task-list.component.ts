import { Component, OnInit } from '@angular/core';
import { TaskService } from '../../services/task.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Task } from 'src/app/modules/task/models/task';
import { Router } from '@angular/router';
import { User } from 'src/app/modules/user/models/user';
import { Project } from 'src/app/modules/project/models/project';
import { UserService } from 'src/app/modules/user/services/user.service';
import { ProjectService } from 'src/app/modules/project/services/project.service';
import { SnackBarService } from 'src/app/modules/shared/services/snack-bar.service';
import { SnackBarMessage } from 'src/app/enums/snackbar-message.enum';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css'],
})
export class TasksListComponent implements OnInit {
  public tasks: Task[];
  public users: User[];
  public projects: Project[];
  public constructor(
    private taskService: TaskService,
    private snackBarService: SnackBarService,
    private userService: UserService,
    private projectService: ProjectService,
    private router: Router
  ) {}
  private unsubscribe$ = new Subject<void>();

  public ngOnInit() {
    this.onDelete = this.onDelete.bind(this);
    this.onChange = this.onChange.bind(this);
    this.taskService
      .getAllTasks()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: (response: any) => {
          this.tasks = response.body;
        },
        error: (error) => {
          this.snackBarService.showErrorMessage(error);
        },
      });

    this.userService
      .getAllUsers()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: (response: any) => {
          this.users = response.body;
        },
        error: (error) => {
          this.snackBarService.showErrorMessage(error);
        },
      });

    this.projectService
      .getAllProjects()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: (response: any) => {
          this.projects = response.body;
        },
        error: (error) => {
          this.snackBarService.showErrorMessage(error);
        },
      });

  }

  public onDelete(id: number) {
    const isDeletingConfirmed = confirm('Are you sure to delete it?');
    if (isDeletingConfirmed) {
      this.taskService
        .DeleteTask(id)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe({
          next: (response: any) => {
            this.tasks = this.tasks.filter(
              (task) => task.id !== id
            );
            this.snackBarService.showSuccessMessage(SnackBarMessage.SUCCESSFULLY_DELETED);
          },
          error: (error) => {
            this.snackBarService.showErrorMessage(error);
          },
        });
  
    }
  }

  public onChange(id: number) {
    this.router.navigate([`/task/${id}/change`]);
  }

  public onCreate() {
    this.router.navigate([`/task/create`]);
  }

  public getPerformerName(performerId: number) {
    const performer = this.users.find((user) => user.id == performerId);
    return `${performer.firstName} ${performer.lastName}`;
  }

  public getProjectName(projectId: number) {
    return this.projects.find((project) => project.id == projectId).name;
  }
}

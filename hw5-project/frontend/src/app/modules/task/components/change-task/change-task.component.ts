import { Component, Input, OnInit } from '@angular/core';
import { TaskService } from 'src/app/modules/task/services/task.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Task } from 'src/app/modules/task/models/task';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UserService } from 'src/app/modules/user/services/user.service';
import { User } from 'src/app/modules/user/models/user';
import { Project } from 'src/app/modules/project/models/project';
import { ProjectService } from 'src/app/modules/project/services/project.service';
import { MIN_NAME_LENGTH } from '../../constants/validation.constants';
import { CustomBoolean } from 'src/app/types/custom-boolean.type';
import { SnackBarService } from 'src/app/modules/shared/services/snack-bar.service';
import { SnackBarMessage } from 'src/app/enums/snackbar-message.enum';

@Component({
  selector: 'app-change-task',
  templateUrl: './change-task.component.html',
  styleUrls: ['./change-task.component.css'],
})
export class ChangeTaskComponent implements OnInit {
  public task = {} as Task;
  public users: User[];
  public projects: Project[];
  public isEditing: boolean;
  public taskId: number;
  public taskForm: FormGroup;
  private isChangesSaved: boolean;
  private unsubscribe$ = new Subject<void>();
  public constructor(
    private snackBarService: SnackBarService,
    private taskService: TaskService,
    activateRoute: ActivatedRoute,
    private userService: UserService,
    private projectService: ProjectService,
    private router: Router
  ) {
    this.isChangesSaved = false;
    this.taskId = activateRoute.snapshot.params['id'];
    this.isEditing = !!this.taskId;
  }

  public canClose(): CustomBoolean {
    return !this.taskForm.dirty || this.isChangesSaved;
  }

  ngOnInit(): void {
    if (this.taskId) {
      this.taskService
        .getTaskById(this.taskId)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe({
          next: (response: any) => {
            this.task = response.body;
          },
          error: (error) => {
            this.snackBarService.showErrorMessage(error);
          },
        });
    }
    this.userService
      .getAllUsers()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: (response: any) => {
          this.users = response.body;
        },
        error: (error) => {
          this.snackBarService.showErrorMessage(error);
        },
      });
    this.projectService
      .getAllProjects()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: (response: any) => {
          this.projects = response.body;
        },
        error: (error) => {
          this.snackBarService.showErrorMessage(error);
        },
      });

    this.taskForm = new FormGroup({
      name: new FormControl(this.task.name, [
        Validators.required,
        Validators.minLength(MIN_NAME_LENGTH),
      ]),
      performer: new FormControl(this.users, Validators.required),
      description: new FormControl(this.task.description, Validators.required),
      project: new FormControl(this.projects, Validators.required),
    });
  }

  updateTask() {
    this.taskService
      .UpdateTask(this.task)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: (response: any) => {
          this.task = response.body;
          this.isChangesSaved = true;
          this.router.navigate([`/task`]);
          this.snackBarService.showSuccessMessage(
            SnackBarMessage.SUCCESSFULLY_UPDATED
          );
        },
        error: (error) => {
          this.snackBarService.showErrorMessage(error);
        },
      });
  }
  createTask() {
    const { description, name, projectId, performerId } = this.task;
    this.taskService
      .AddTask({ description, name, projectId, performerId })
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: (response: any) => {
          this.task = response.body;
          this.isChangesSaved = true;
          this.router.navigate([`/task`]);
          this.snackBarService.showSuccessMessage(
            SnackBarMessage.SUCCESSFULLY_CREATED
          );
        },
        error: (error) => {
          this.snackBarService.showErrorMessage(error);
        },
      });
  }
  public handleCancel() {
    this.router.navigate([`/task`]);
  }
}

import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { TasksListComponent } from './components/task-list/task-list.component';
import { BrowserModule } from '@angular/platform-browser';
import { ChangeTaskComponent } from './components/change-task/change-task.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { TaskStateDirective } from './directives/task-state.directive';

@NgModule({
  declarations: [TasksListComponent, ChangeTaskComponent, TaskStateDirective],
  imports: [
    SharedModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
  ],
  exports: [TasksListComponent],
})
export class TaskModule {}

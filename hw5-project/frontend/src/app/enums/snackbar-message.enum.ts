export enum SnackBarMessage {
  SUCCESSFULLY_UPDATED = 'Successfully updated',
  SUCCESSFULLY_CREATED = 'Successfully created',
  SUCCESSFULLY_DELETED = 'Successfully deleted',
}

import { Routes } from '@angular/router';
import { ChangeProjectComponent } from './modules/project/components/change-project/change-project.component';
import { ProjectsListComponent } from './modules/project/components/project-list/project-list.component';
import { CanCloseGuard } from './modules/shared/guards/can-close.guard';
import { ChangeTaskComponent } from './modules/task/components/change-task/change-task.component';
import { TasksListComponent } from './modules/task/components/task-list/task-list.component';
import { ChangeTeamComponent } from './modules/team/components/change-team/change-team.component';
import { TeamsListComponent } from './modules/team/components/team-list/team-list.component';
import { ChangeUserComponent } from './modules/user/components/change-user/change-user.component';
import { UsersListComponent } from './modules/user/components/user-list/user-list.component';

export const AppRoutes: Routes = [
  { path: 'task', component: TasksListComponent, pathMatch: 'full' },
  {
    path: 'task/:id/change',
    component: ChangeTaskComponent,
    pathMatch: 'full',
    canDeactivate: [CanCloseGuard],
  },
  { path: 'task/create', component: ChangeTaskComponent, pathMatch: 'full' },
  {
    path: 'team/:id/change',
    component: ChangeTeamComponent,
    pathMatch: 'full',
    canDeactivate: [CanCloseGuard],
  },
  { path: 'team/create', component: ChangeTeamComponent, pathMatch: 'full' },

  { path: 'team', component: TeamsListComponent, pathMatch: 'full' },
  { path: 'user', component: UsersListComponent, pathMatch: 'full' },
  { path: 'user/create', component: ChangeUserComponent, pathMatch: 'full' },
  {
    path: 'user/:id/change',
    component: ChangeUserComponent,
    pathMatch: 'full',
    canDeactivate: [CanCloseGuard],
  },
  { path: 'project', component: ProjectsListComponent, pathMatch: 'full' },
  {
    path: 'project/create',
    component: ChangeProjectComponent,
    pathMatch: 'full',
  },
  {
    path: 'project/:id/change',
    component: ChangeProjectComponent,
    pathMatch: 'full',
    canDeactivate: [CanCloseGuard],
  },
  { path: '**', redirectTo: '' },
];

import { Observable } from "rxjs";

export type CustomBoolean = Observable<boolean> | boolean;